/*
 *  $Id: NotePlayer.h 3269 2005-05-14 07:11:50Z leighsmith $
 *  MusicKit
 *
 *  Created by Leigh Smith on 4/25/05.
 *
 *  Copyright (c) 2005, The MusicKit Project.  All rights reserved.
 *
 *  Permission is granted to use and modify this code for commercial and 
 *  non-commercial purposes so long as the author attribution and copyright 
 *  messages remain intact and accompany all relevant code.
 *
 */

@interface NotePlayer: NSObject
{
    
}

- play: sender;

- setFreqFrom: sender;

+ initialize;

- init;

@end
