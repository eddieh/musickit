// InfoPanelController.h

#import <AppKit/AppKit.h>

@interface InfoPanelController : NSWindowController {
}

- (id) init;
- (IBAction) showWindow: (id) sender;

@end
