/*
  $Id: ScoreRecorderPrivate.h 3250 2005-05-11 02:16:30Z leighsmith $
  Defined In: The MusicKit
 
  Copyright (c) 1988-1992, NeXT Computer, Inc.
  Portions Copyright (c) 1999-2005, The MusicKit Project.
*/
#ifndef __MK__ScoreRecorder_H___
#define __MK__ScoreRecorder_H___

#import "MKScoreRecorder.h"

@interface MKScoreRecorder(Private)

- (void) _firstNote: (MKNote *) aNote;
- _afterPerformance;

@end

#endif
