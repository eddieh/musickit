/*
  $Id: trigonometry.h 2325 2002-09-25 22:28:52Z leighsmith $
  Defined In: The MusicKit

  Description:
    Simple trigonometric functions using a sine table of size 1024
    with linear interpolation.
 
  Original Author: Leigh Smith

  Copyright (c) 1999-2002 The MusicKit Project.
*/

double MKSine(double x);
double MKCosine(double x);

